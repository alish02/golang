package main

import "fmt"

func main() {

	array := []string{"a", "b", "c", "c", "c", "a"}
	search1(array)
	search2(array)
}

func search1(words []string) {
	var word string
	quantity := 0

	for i := range words {
		currnetQuantity := 0
		for j := range words {

			if words[i] == words[j] {
				currnetQuantity++
			}
		}

		if currnetQuantity > quantity {
			word = words[i]
			quantity = currnetQuantity
		}
	}

	fmt.Println(word, quantity)
}

/*
все также поиск в массиве (массив внутри массива)
*/

func search2(words []string) {
	hashMap := make(map[string]int)

	for i := range words {
		if _, ok := hashMap[words[i]]; !ok {
			hashMap[words[i]] = 0
		}
		hashMap[words[i]]++
	}

	fmt.Println(hashMap) // TODO: надо доделать
}

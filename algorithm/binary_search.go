package main

import (
	"fmt"
	"math/rand"
)

func main() {
	start := 1
	end := 15
	array := generateArray(start, end)
	num := randNum(start, end)
	result := binarySearch(num+5, array)
	fmt.Printf("key: %d value: %d\n", result[0], result[1])
}

func generateArray(start int, end int) (result []int) {
	for i := start; i <= end; i++ {
		result = append(result, i)
	}
	return result // рандомные числа
}

func randNum(min int, max int) int {
	result := rand.Intn(max-min) + min
	return result // рандомное число для поиска
}

func binarySearch(item int, array []int) (result []int) {
	start := 0            // всегда начинаем с 0
	end := len(array) - 1 // конец

	if item <= array[end] { // проверка на то что такой элемент не больше array[end]
		for start <= end {
			middle := (start + end) / 2 // делим на 2 (таким образом получем середину)

			if array[middle] == item {
				/*
					здесь очень важно проверять по ключу => array[middle], почему?
					пример: array = [1,2]; item = 2
					если needle == haystack[last] то вернет null
				*/
				result = append(result, middle)        // key
				result = append(result, array[middle]) // value
				break                                  // выход
			} else if array[middle] < item { // если меньше, то добавляем
				start = middle + 1
			} else if array[middle] > item { // если меньше, то отнимаем
				end = middle - 1
			}
		}
	} else {
		result = append(result, -1)
		result = append(result, -1)
	}
	return result
}

/*
функция binarySearch получает отсортированный массив и значенине. если значение присутствует в массиве, то возвращает его позицию.
*/

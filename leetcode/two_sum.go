package main

import (
	"fmt"
)

func main() {
	array := []int{2, 7, 11, 15}
	target := 9
	result1 := twoSum1(array, target)
	result2 := twoSum2(array, target)
	result3 := twoSum3(array, target)
	fmt.Println(result1)
	fmt.Println(result2)
	fmt.Println(result3)
}

func twoSum1(nums []int, target int) []int {
	for i, first := range nums {
		for j, second := range nums {
			if (first+second) == target && i != j {
				return []int{i, j}
			}
		}
	}

	return []int{0, 0}
}

/*
цикл внутри цикла, линейное время. Очень долго
*/

func twoSum2(nums []int, target int) []int {
	hashMap := make(map[int]int) // инициализация хэш таблицы

	for i := range nums {
		hashMap[nums[i]] = i // заполняю таблицу
	}

	for i := range nums {
		remainder := target - nums[i]     // остаток
		key, status := hashMap[remainder] // возвращает сам элемент и true/false

		if status && i != key {
			return []int{i, key}
		}
	}

	return []int{0, 0}
}

/*
реализовано с помощью хэш таблицы, не плохо
*/

func twoSum3(nums []int, target int) []int {
	hashMap := make(map[int]int) // инициализация хэш таблицы

	for i := range nums {
		remainder := target - nums[i]     // остаток
		key, status := hashMap[remainder] // возвращает сам элемент и true/false

		if status && i != key {
			return []int{i, key}
		}
		hashMap[nums[i]] = i

	}
	return []int{0, 0}
}
